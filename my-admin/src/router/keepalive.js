import { empty } from "@wzh-/utils";
import { ref, watch } from "vue";

//处理页面缓存
/**
 * 如何缓存页面: 当页面设置了 name 属性
 * 如何清除缓存: 调用 removeKeepalive 主动删除
 */
export const keepaliveList = ref([]);

/**
 * 删除缓存
 * @param name { string }
 */

export function removeKeepalive(name) {
  keepaliveList.value = keepaliveList.value.filter((item) => item !== name);
}

// 返回标识
let usePopstate = false;

// 监听浏览器的前进和后退 打上返回标识
// history.back()、history.forward()、history.go()
// router.back()、router.go()
window.addEventListener("popstate", () => {
  usePopstate = true;
});

/**
 *
 * @param {*} to 当前路由
 * @param {*} from 上一个路由
 * @param {*} failure 错误信息
 * @returns
 */
export default async function (to, from, failure) {
  // redirect时
  if (to.name === from.name) {
    return true;
  }

  // keepaliveList 状态卡位
  await new Promise((resolve) => setTimeout(resolve));

  const items = [...keepaliveList.value];

  const toPageName = to.matched.at(-1)?.components?.default?.name;
  //name是必须的
  if (!toPageName) return;
  const index = items.findIndex((item) => item === toPageName);
  if (index > -1) {
    items.splice(index, 1);
  }
  keepaliveList.value = items.concat(toPageName);
  console.log(keepaliveList.value, "keepaliveList");

}

watch(keepaliveList, (items) =>
  console.log("%c更新页面缓存", "color:#33c648", items)
);
